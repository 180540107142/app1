package com.test.app1;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.lang.reflect.Array;

public class CalculatorActivity extends AppCompatActivity {

    Button btnNo1, btnNo2, btnNo3, btnNo4, btnNo5, btnNo6, btnNo7, btnNo8, btnNo9, btnNo0, btnEmt, btnClr, btnBck, btnPnt;
    TextView txtScreen;
    String t = "0";
    int cnt = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.calculator);

        btnNo1 = findViewById(R.id.btnActNo1);
        btnNo2 = findViewById(R.id.btnActNo2);
        btnNo3 = findViewById(R.id.btnActNo3);
        btnNo4 = findViewById(R.id.btnActNo4);
        btnNo5 = findViewById(R.id.btnActNo5);
        btnNo6 = findViewById(R.id.btnActNo6);
        btnNo7 = findViewById(R.id.btnActNo7);
        btnNo8 = findViewById(R.id.btnActNo8);
        btnNo9 = findViewById(R.id.btnActNo9);
        btnNo0 = findViewById(R.id.btnActNo0);
        btnEmt = findViewById(R.id.btnActEmt);
        btnClr = findViewById(R.id.btnActClr);
        btnBck = findViewById(R.id.btnActBck);
        btnPnt = findViewById(R.id.btnActPnt);
        txtScreen = findViewById(R.id.txtActScreen);

        clicklistner();
    }

    void clicklistner(){
        btnNo1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView tv = (TextView)findViewById(R.id.txtActScreen);
                String screen = tv.getText().toString();
                if(screen.equals(t))
                {
                    int num = 1;
                    txtScreen.setText(String.valueOf(num));
                }
                else {
                    int num = 1;
                    txtScreen.append(String.valueOf(num));
                }
            }
        });
        btnNo2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView tv = (TextView)findViewById(R.id.txtActScreen);
                String screen = tv.getText().toString();
                if(screen.equals(t))
                {
                    int num = 2;
                    txtScreen.setText(String.valueOf(num));
                }
                else {
                    int num = 2;
                    txtScreen.append(String.valueOf(num));
                }
            }
        });
        btnNo3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView tv = (TextView)findViewById(R.id.txtActScreen);
                String screen = tv.getText().toString();
                if(screen.equals(t))
                {
                    int num = 3;
                    txtScreen.setText(String.valueOf(num));
                }
                else {
                    int num = 3;
                    txtScreen.append(String.valueOf(num));
                }
            }
        });
        btnNo4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView tv = (TextView)findViewById(R.id.txtActScreen);
                String screen = tv.getText().toString();
                if(screen.equals(t))
                {
                    int num = 4;
                    txtScreen.setText(String.valueOf(num));
                }
                else {
                    int num = 4;
                    txtScreen.append(String.valueOf(num));
                }
            }
        });
        btnNo5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView tv = (TextView)findViewById(R.id.txtActScreen);
                String screen = tv.getText().toString();
                if(screen.equals(t))
                {
                    int num = 5;
                    txtScreen.setText(String.valueOf(num));
                }
                else {
                    int num = 5;
                    txtScreen.append(String.valueOf(num));
                }
            }
        });
        btnNo6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView tv = (TextView)findViewById(R.id.txtActScreen);
                String screen = tv.getText().toString();
                if(screen.equals(t))
                {
                    int num = 6;
                    txtScreen.setText(String.valueOf(num));
                }
                else {
                    int num = 6;
                    txtScreen.append(String.valueOf(num));
                }
            }
        });
        btnNo7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView tv = (TextView)findViewById(R.id.txtActScreen);
                String screen = tv.getText().toString();
                if(screen.equals(t))
                {
                    int num = 7;
                    txtScreen.setText(String.valueOf(num));
                }
                else {
                    int num = 7;
                    txtScreen.append(String.valueOf(num));
                }
            }
        });
        btnNo8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView tv = (TextView)findViewById(R.id.txtActScreen);
                String screen = tv.getText().toString();
                if(screen.equals(t))
                {
                    int num = 8;
                    txtScreen.setText(String.valueOf(num));
                }
                else {
                    int num = 8;
                    txtScreen.append(String.valueOf(num));
                }
            }
        });
        btnNo9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView tv = (TextView)findViewById(R.id.txtActScreen);
                String screen = tv.getText().toString();
                if(screen.equals(t))
                {
                    int num = 9;
                    txtScreen.setText(String.valueOf(num));
                }
                else {
                    int num = 9;
                    txtScreen.append(String.valueOf(num));
                }
            }
        });
        btnNo0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView tv = (TextView)findViewById(R.id.txtActScreen);
                String screen = tv.getText().toString();
                if(screen.equals(t))
                {
                    int num = 0;
                    txtScreen.setText(String.valueOf(num));
                }
                else {
                    int num = 0;
                    txtScreen.append(String.valueOf(num));
                }
            }
        });
        btnEmt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(cnt>0)
                {
                    cnt=0;
                }
                int num = 0;
                txtScreen.setText(String.valueOf(num));
            }
        });
        btnClr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(cnt>0)
                {
                    cnt=0;
                }
                String sc = "0";
                txtScreen.setText(sc);
            }
        });
        btnBck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(cnt==0)
                {
                    TextView tv = (TextView)findViewById(R.id.txtActScreen);
                    String TextFromtv = tv.getText().toString();
                    int IntFromtv = new Integer(TextFromtv).intValue();
                    IntFromtv = IntFromtv/10;
                    txtScreen.setText(String.valueOf(IntFromtv));
                }
            }
        });
        btnPnt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(cnt==0)
                {
                    cnt = 1;
                    String pnt = ".";
                    txtScreen.append(pnt);
                }
            }
        });
    }
}
