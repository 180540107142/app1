package com.test.app1.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class TblUserdata extends Mydatabase {

    public static final String TBL_USER_DATA = "Tbl_Userdata";
    public static final String NAME = "Name";
    public static final String PHONE = "Phone";
    public static final String EMAIL = "Email";

    public TblUserdata(Context context) {
        super(context);
    }

    public long insertUserDetails(String Name, String Phone, String Email){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(NAME, Name);
        cv.put(PHONE, Phone);
        cv.put(EMAIL, Email);
        long insertedID = db.insert(TBL_USER_DATA,"exp", cv);
        db.close();
        return insertedID;
    }
}
