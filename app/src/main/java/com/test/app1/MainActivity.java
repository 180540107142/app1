package com.test.app1;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.test.app1.database.Mydatabase;
import com.test.app1.database.TblUserdata;
import com.test.app1.util.Constant;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

        EditText etFirstName,etLastName, etPhone, etEmail;
        Button btnSubmit,btnCalC ,btnListview ;
        TextView lblNameView;
        ImageView imgBackground;
        CheckBox chbxSwimming, chbxPainting, chbxDancing;
        RadioButton rbtnMale, rbtnFemale;
        ArrayList<HashMap<String,Object>> arrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo);
        initViewReference();

        etFirstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.d("Text changed::" , "before" + charSequence);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.d("Text changed::" , "on" + charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {
                Log.d("Text changed::" , "after" + editable);
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String Name = etFirstName.getText().toString() + " " + etLastName.getText().toString();
                String Phone = etPhone.getText().toString();
                String Email = etEmail.getText().toString();
//                Intent intent = new Intent(MainActivity.this,SecondActivity.class);
//                intent.putExtra("namedetail", namedetail);
//                startActivity(intent);
                TblUserdata tblUserdata = new TblUserdata(MainActivity.this);
                long insertedID = tblUserdata.insertUserDetails(Name,Phone,Email);
                Toast.makeText(getApplicationContext(), insertedID > 0 ? "done" : "not done", Toast.LENGTH_SHORT).show();
            }
        });

        btnCalC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(MainActivity.this,CalculatorActivity.class));
            }
        });

        btnListview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isValid()){
                    HashMap<String,Object> userlist = new HashMap<>();
                    userlist.put(Constant.first_name, etFirstName.getText().toString());
                    userlist.put(Constant.last_name, etLastName.getText().toString());
                    userlist.put(Constant.phone, etPhone.getText().toString());
                    userlist.put(Constant.email, etEmail.getText().toString());

                    String gender = "";
                    if(rbtnMale.isChecked())
                    {
                        gender = "M";
                        userlist.put(Constant.gender, gender);
                    }
                    else if(rbtnFemale.isChecked())
                    {
                        gender = "F";
                        userlist.put(Constant.gender, gender);
                    }

                    String hobbies = "";
                    if(chbxSwimming.isChecked()){
                        hobbies += " " +chbxSwimming.getText().toString();
                    }
                    if(chbxPainting.isChecked()){
                        hobbies += " "+chbxPainting.getText().toString();
                    }
                    if(chbxDancing.isChecked()){
                        hobbies += " "+chbxDancing.getText().toString();
                    }
                    if(hobbies.length()>0)
                    {
                        hobbies = hobbies.substring(1);
                    }
                    userlist.put(Constant.hobby, hobbies);

                    arrayList.add(userlist);
                    Intent intent = new Intent(MainActivity.this,ListviewActivity.class);
                    intent.putExtra("List_of_user_detail", arrayList);
                    startActivity(intent);
                }

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.dashboard_menu, menu);
        return true;
    }

    void initViewReference(){
        new Mydatabase(MainActivity.this).getReadableDatabase();

        etFirstName = findViewById(R.id.etActFirstName);
        etLastName = findViewById(R.id.etActLastName);
        btnSubmit = findViewById(R.id.btnActSubmit);
        btnCalC = findViewById(R.id.btnActCalc);
        lblNameView = findViewById(R.id.lblActNameView);
        imgBackground = findViewById(R.id.imgActBackground);
        btnListview = findViewById(R.id.btnActListView);
        chbxSwimming = findViewById(R.id.chbxActSwimming);
        chbxPainting = findViewById(R.id.chbxActPainting);
        chbxDancing = findViewById(R.id.chbxActDancing);
        rbtnMale = findViewById(R.id.rbtnActMale);
        rbtnFemale = findViewById(R.id.rbtnActFemale);
        etPhone = findViewById(R.id.etActPhn);
        etEmail = findViewById(R.id.etActEmail);
    }

    boolean isValid(){
        boolean flag = true;

        //Name error
        if(TextUtils.isEmpty(etFirstName.getText())){
            etFirstName.setError("Enter proper value");
            flag = false;
        }
        if(TextUtils.isEmpty(etLastName.getText())){
            etLastName.setError("Enter proper value");
            flag = false;
        }

        //Phone error
        if(TextUtils.isEmpty(etPhone.getText())){
            etPhone.setError("Enter proper value");
            flag = false;
        }
        else{
            String phone = etPhone.getText().toString();
            if(phone.length()<10){
                etPhone.setError("Enter 10 Digit");
                flag = false;
            }
        }

        //Email error
        if(TextUtils.isEmpty(etEmail.getText())){
            etEmail.setError("Enter proper value");
            flag = false;
        }
        else {
            String email = etEmail.getText().toString();
            String emailpattern = "[a-zA-Z0-9]+@[a-z]+\\.+[a-z]+";
            if(!email.matches(emailpattern)){
                etPhone.setError("Enter proper value");
                flag = false;
            }
        }

        return  flag;
    }
}