package com.test.app1;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.test.app1.util.Constant;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class ListviewActivity extends AppCompatActivity {

    ListView userlist;
    ArrayList<HashMap<String,Object>> arrayList = new ArrayList<>();
    UserListAdapter userListAdapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listview);
        initViewReference();
        bindViewValues();
    }

    void bindViewValues(){
        arrayList.addAll((Collection<? extends HashMap<String, Object>>)getIntent().getSerializableExtra("List_of_user_detail"));
        userListAdapter = new UserListAdapter(this,arrayList);
        userlist.setAdapter(userListAdapter);

        userlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Toast.makeText(ListviewActivity.this, "This is the Phone no.!!!\n"+ arrayList.get(position).get(Constant.phone).toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    void initViewReference(){
        userlist = findViewById(R.id.lstView);
    }
}
