package com.test.app1;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.test.app1.util.Constant;

import java.util.ArrayList;
import java.util.HashMap;

public class UserListAdapter extends BaseAdapter {

    Context context;
    ArrayList<HashMap<String,Object>> arrayList;

    public UserListAdapter(Context context, ArrayList<HashMap<String,Object>> arrayList){
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View viewlist = LayoutInflater.from(context).inflate(R.layout.activity_second, null);
        TextView tvfname = viewlist.findViewById(R.id.tvActfisrtname);
        TextView tvphone = viewlist.findViewById(R.id.tvActPhone);
        TextView tvemail = viewlist.findViewById(R.id.tvActEmail);
        TextView tvgender = viewlist.findViewById(R.id.tvActGender);
        TextView tvhobby = viewlist.findViewById(R.id.tvActhobbies);

        if(arrayList.get(position).get(Constant.gender)=="M")
        {
            String M = "M";
            tvgender.setText(M);
        }
        else if(arrayList.get(position).get(Constant.gender)=="F")
        {
            String M = "F";
            tvgender.setText(M);
        }
        tvfname.setText(String.valueOf(arrayList.get(position).get(Constant.first_name)) + " " +String.valueOf(arrayList.get(position).get(Constant.last_name)));
        tvphone.setText(String.valueOf(arrayList.get(position).get(Constant.phone)));
        tvemail.setText(String.valueOf(arrayList.get(position).get(Constant.email)));
        tvgender.setText(String.valueOf(arrayList.get(position).get(Constant.gender)));
        tvhobby.setText(String.valueOf(arrayList.get(position).get(Constant.hobby)));
        return viewlist;
    }
}
