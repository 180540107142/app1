package com.test.app1.util;

public class Constant {

    public static final String first_name = "firstname";
    public static final String last_name = "lastname";
    public static final String phone = "phone";
    public static final String email = "email";
    public static final String gender = "gender";
    public static final String hobby = "hobby";

}
